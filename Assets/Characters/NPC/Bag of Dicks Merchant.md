Bag of Dicks Merchant
=====================

The bag of dicks merchant is a violent pushy type of merchant, with a peculiar penchant for targeting Jordan in particular for his pushy sales pitches.

His back story is that he invested **HEAVILY**, (read: every last dime he had) into stocks of bag of dicks, (in addition to becoming an independent salesman therefore) and canonically the stock has gone sideways, slowly declining, and the merchant is stuck in sunk cost mentality, and therefore becomes increasingly desperate to make as many sales as possible.
